<!--#include virtual="inc/header.inc"-->
<!DOCTYPE html>
<html id="">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title><%= Application.Contents("apptitle") %></title>
        <link href="/img/favicon.ico" rel="shortcut icon" type="image/png" />
        <link rel="stylesheet" href="/css/main.css" crossorigin="anonymous">
    </head>

    <body id="registrationpage">
        <form id="my-form" method="post" action="submit_registration.asp">
            <fieldset>
                <legend>Register for an account <a style="float:right;" href="/">Back to login</a></legend>
                <div class="formgrid">
                    <input id="name" name="name" type="text"/>
                    <label for="name">name</label>
                    <input id="email" name="email" type="text"/>
                    <label for="email">e-mail address</label>
                    <input id="password" name="password" type="password"/>
                    <label for="password">password</label>
                    <button type="submit" name="submit" value="true">SUBMIT</button>
                </div>
            </fieldset>
        </form>
    <footer></footer>
    </body>
</html>
<!--#include virtual="inc/footer.inc"-->