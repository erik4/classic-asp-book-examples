<%
' An example of using the syslog WSC
' Have a look at the source code of the WSC itself to see what properties and methods are available.
dim syslogger
Set	syslogger = GetObject("script:"&Server.MapPath("/lib/syslog.wsc"))
    syslogger.open("udp://localhost:514")
    syslogger.Error("This is an error message")
    syslogger.Info("This is an info message")
    syslogger.SyslogVersion = "3164"
    syslogger.Alert("Changed to syslog version 3164")
    syslogger.loglevel = 3
    syslogger.Error("At loglevel 3, an error will register")
    syslogger.Warning("At loglevel 3, a warning will NOT register")
    syslogger.close()
Set syslogger = Nothing    
%>