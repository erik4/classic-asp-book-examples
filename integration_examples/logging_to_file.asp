<%
    ' let's keep a log file for ten days
    Application.Contents("LogRetention") = "10"
    dim logger
    set logger = GetObject("script:"&Server.MapPath("/lib/logger.wsc"))
        logger.logfile = "Test.log"
        logger.write("This is a test, writing to a logfile called test.log in a subfolder called ""log\"".")
        logger.write("Make sure that folder exists and you have write rights there.")
        logger.logfile = ""
        logger.write("Writing to a log file that has the current datestamp as a name")
    set logger = nothing
%>
