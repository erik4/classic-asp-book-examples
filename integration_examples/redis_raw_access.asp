<%
Dim Assert
Set Assert = GetObject("script:" & Server.Mappath("/lib/unittest.wsc"))
Dim Redis
    
function Initialize()
    Set Redis = Server.CreateObject("RedisComClient")
    Redis.Open("localhost")
end function

function test_SetPermanent_and_Get()
    call Redis.SetPermanent("key1", "abcde")
    call Assert.AreEqual("abcde", Redis.Get("key1"))
end function

function test_Del()
    call Redis.SetPermanent("key1", "abcde")
    Redis.Del("key1")
    Assert.IsNullOrEmpty(Redis.Get("key1"))
end function

function test_CheckTTL()
    call Redis.SetPermanent("dk", "123")
    call Redis.Expire("dk", 10)
    dim timetolive : timetolive = Redis.TTL("dk")
    Assert.IsTrue(timetolive >= 5 and timetolive <= 10)
end function

function test_Set_and_Persist()
    call Redis.Set("persistentkey", "abcde", 90)
    Redis.Persist("persistentkey")
    dim timetolive : timetolive = Redis.TTL("persistentkey")
    Assert.IsTrue(timetolive = -1)
end function

function test_string_type()
    call Redis.Set("stringkey", "abcde", 10)
    dim valuetype : valuetype = Redis.Type("stringkey")
    Assert.IsTrue(lcase(valuetype) = "string")
end function

function test_incr()
    call Redis.Set("MyNumber", 3, 10)
    dim newValue : newValue = Redis.Incr("MyNumber")
    Assert.IsTrue(newValue = 4)
    call Assert.AreEqual("4", Redis.Get("MyNumber"))
end function

function test_Hset_Hget()
    call Redis.Hset("testhash", "firstfield", "firstvalue")
    call Redis.Expire("testhash", 60)
    dim result : result = Redis.Hget("testhash", "firstfield")
    call Assert.AreEqual("firstvalue", result)
end function

function test_GetByObject()
    call Redis.SetPermanent("dk", "123")
    call Assert.AreEqual("123", Redis("dk"))
end function

function test_SetByObject()
    Redis("dk") = "123"
    call Assert.AreEqual("123", Redis("dk"))
end function

function test_RemoveAllFromThisKey()
    dim Redis2 : Set Redis2 = Server.CreateObject("RedisComClient")
    Redis2.Open("localhost")
    call Redis2.SetPermanent("myPrefix2:firstname", "22222")
    call Redis2.SetPermanent("myPrefix2:lastname", "33333")

    call Redis.SetPermanent("myPrefix:firstname", "firstname123")
    call Redis.SetPermanent("myPrefix:lastname", "lastname123")
    Redis.RemoveKeysWithPrefix("myPrefix:")

    Assert.IsNullOrEmpty(Redis("myPrefix:firstname"))
    Assert.IsNullOrEmpty(Redis("myPrefix:lastname"))
    Assert.IsNotNullOrEmpty(Redis2("myPrefix2:firstname"))
    Assert.IsNotNullOrEmpty(Redis2("myPrefix2:lastname"))
    Set Redis2 = Nothing
end function

function test_Exists()
    call Redis.SetPermanent("exists", "12344")
    Assert.IsTrue(Redis.Exists("exists"))
end function

function test_NotExists()
    Assert.IsFalse(Redis.Exists("notexists"))
end function

function test_Remove()
    call Redis.SetPermanent("onekey", "12344")
    Redis.Del("onekey")
    Assert.IsFalse(Redis.Exists("onekey"))
end function

function test_SetExpirationExistingKey()
    call Redis.SetPermanent("key4", "12344")
    Assert.IsTrue(Redis.Exists("key4"))
    call Redis.SetExpiration("key4",1000)
    Assert.IsTrue(Redis.TTL("key4") <= 1000)
end function

function test_AddNullValue()
    call Redis.SetPermanent("null", null)
    Assert.IsTrue(Redis.Exists("null"))
end function

function test_AddSameKeyTwice()
    Redis("twice") = 1
    Redis("twice") = "asdf"
    call Assert.AreEqual("asdf", Redis("twice"))
end function

function finalize()
    Set Redis = Nothing    
end function

' *******************************************************************************
' *                             HERE ARE THE TESTS                              *
' *******************************************************************************

    Initialize()
    
    test_SetPermanent_and_Get()
    test_Del()
    test_CheckTTL()
    test_Set_and_Persist()
    test_string_type()
    test_incr()
    test_Hset_Hget()
    test_GetByObject()
    test_SetByObject()
    test_RemoveAllFromThisKey()
    test_Exists()
    test_NotExists()
    test_Remove()
    test_SetExpirationExistingKey()
    test_AddNullValue()
    test_AddSameKeyTwice()
    
    finalize()

Set Assert = Nothing

' *******************************************************************************
' *                               EXAMPLE USAGE                                 *
' *******************************************************************************

Set Redis = Server.CreateObject("RedisComClient")
    ' Pass in a configuration to connect to a server
    Redis.Open("localhost")
    
    ' SetPermanent gives a key an indefinite TTL...
    call Redis.SetPermanent("key1", "abcde")
    Response.Write("Key1 = " & Redis.Get("key1") & "<br/>")
    
    ' ...but you can always change the TTL later
    call Redis.Expire("key1", 60)
    Response.Write("Key1 TTL = " & Redis.TTL("key1") & "<br/>")
    
    ' Or delete it alltogether
    Redis.Del("key1")
    Response.Write("Key1 is now gone... " & Redis("key1") & "<br/>")
    
    ' The key "stringkey" will have a TTL of 10 seconds
    call Redis.Set("stringkey", "abcde", 10)
    
    ' We can also put keys and values in hashes...
    call Redis.Hset("testhash", "firstfield", "firstvalue")
    call Redis.Hset("testhash", "secondfield", "secondvalue")
    Response.Write("Testhash[firstfield] = " & Redis.Hget("testhash", "firstfield") & "<br/>")
    Response.Write("Testhash[secondfield] = " & Redis.Hget("testhash", "secondfield") & "<br/>")
    
    ' ...and expire the entire hash
    call Redis.Expire("testhash", 60)
    Response.Write("Testhash will expire in 60 seconds. The TTL is:" & Redis.TTL("testhash") & "<br/>")
    dim result : result = Redis.Hget("testhash", "firstfield")
    Response.Write("Here is our Testhash[firstfield] again:" & result & "<br/>")

    ' Or delete a complete hash in one go
    Redis.Del("testhash")
    Response.Write("Testhash is now gone...<br/>")
Set Redis = Nothing

%>
