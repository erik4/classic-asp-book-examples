<%
    Dim rSession
    Set rSession = GetObject("script:" & Server.Mappath("/lib/redissession.wsc"))
        rSession.open()
        rSession.Store "session_string","value of the session variable"
        rSession.Store "session_number", 3.14
        rSession.Store "session_boolean", true
        
        Response.Write rSession.Retrieve("session_string") & " : " & TypeName(rSession.Retrieve("session_string")) & "<br>"
        Response.Write rSession.Retrieve("session_number") & " : " & TypeName(rSession.Retrieve("session_number")) & "<br>"
        Response.Write rSession.Retrieve("session_boolean") & " : " & TypeName(rSession.Retrieve("session_boolean")) & "<br>"

        rSession.close()
    Set rSession = Nothing
%>