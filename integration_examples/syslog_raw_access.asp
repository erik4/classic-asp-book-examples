<% 
' A very simple example of using the syslog COM+/ActiveX component in its raw form
' Please read the documentation of the original repository for API details
Set Syslog = Server.CreateObject("SyslogClient")
    Syslog.Open("udp://localhost:514")
    call Syslog.Send5424(1, 3, "ASP", "APP", "This is a simple 5424 test to localhost")
    call Syslog.Send3164(1, 3, "This is a simple 3164 test to localhost")
    Response.Write("Sent Syslog message")
Set Syslog = Nothing
%>