<!--#include virtual="inc/header.inc"-->
<!DOCTYPE html>
<html id="">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <title><%= Application.Contents("apptitle") %></title>
        <link href="/img/favicon.ico" rel="shortcut icon" type="image/png" />
        <link rel="stylesheet" href="/css/main.css" crossorigin="anonymous">
    </head>

    <body id="loginpage">
        <% if rSession.Retrieve("username")<>"" Then Response.Write("Logged in as: <b>" & rSession.Retrieve("username") & "</b>") %>
        <form id="my-form" method="post" action="submit.asp">
            <fieldset>
                <legend><%= T.Label("login") %> <a style="float:right;" href="/register.asp"><%= T.Label("new user") %></a></legend>
                <div class="formgrid">
                    <input id="email" name="email" type="text"/>
                    <label for="email"><%= T.Label("e-mail address") %></label>
                    <input id="password" name="password" type="password"/>
                    <label for="password"><%= T.Label("password") %></label>
                    <button type="submit" name="submit" value="true"><%= T.Label("submit") %></button>
                </div>
            </fieldset>
        </form>
    <footer><%= T.Label("time elapsed") %>:&nbsp;<%= logger.time() %> ms.</footer>
    </body>
</html>
<% logger.Debug("default page was loaded.") %>
<!--#include virtual="inc/footer.inc"-->