### ASPTEST advanced Classic ASP project

This repository belongs to the book [*MAINTAINING AND REFACTORING CLASSIC ASP (ASP 3.0) LEGACY CODE*](https://www.amazon.com/dp/B0846NL485) , by Erik Oosterwaal, available on Amazon.

This book addresses existing legacy classic ASP projects or ASP 3.0 codebases that need te be maintained for whatever reason. The target audience is (former) classic ASP developers or administrators in charge of keeping an existing classic ASP project secure and running.

The book contains tips, tricks and best practices to keep these classic ASP sites up to date and maintainable. It describes step-by-step how to update an existing classic ASP codebase to modern standards and how to keep it monitorable, maintainable and secure.

In the book, you will find a detailed explanation of the contents of this repository, so if you want a detailed explanation and understanding of what's going on, I recommend you buy the book.

[![](./cover_small.png)]((https://www.amazon.com/dp/B0846NL485))

#### Features

This project is used to illustrate the chapters and examples from the book, and contains the following subjects, integrated into the code:

* The use of WSCs (Windows Script Components) in classic ASP to get a tiered, maintainable classic ASP application that supports the DRY principle and separation of concerns. It expands upon this by providing a simple ORM fro classic ASP.
* Logging; both to a local file, and/or to syslog, using an ActiveX/COM+ syslog component.
* A custom session backend: a custom session backend is implemented, that uses Redis for storage. This solves the problem of using webgardens in classic ASP . Normally enabling more worker processes in IIS (AKA creating a webgarden) will mess up the classic ASP session scope, because each worker will have its own session store. The custom unified session backend will make sure multiple processes can access the same session variables in classic ASP.
  Furthermore this allows classic ASP to scale across servers, for the same reasons. 
* Caching in Redis: Redis is also used to implement a caching mechanism for queries. The project has a data access layer in the form of a WSC. This DAL has the possibility to cache queries for speed. The same principle can be applied to other data ofcourse, using the same Redis cache.
* Generic error handler: in the root you'll find 500-100.asp which can be bound to errors of type 500.100. This is already configured in the web.config and it will send a syslog message for every ASP error that occurs.
* Internationalization: using includes, all pages use the UTF-8 charset and codepage by default.
* SQL injection prevention: The tiered setup uses a component that will remove unsafe characters from SQL queries, mitigating SQL injection attacks. Please note that this method of preventing SQL injection attacks is not as safe as using parametrized queries, but easier to implement in existing classic ASP codebases.
* ORM: the WSC components form an ORM for classic ASP. The lib/datamodels folder holds an example of a component that acts as an ORM object. Adding more objects should be trivial using this as an example.
* Hashing: passwords are hashed using SHA-512, by using standard .NET components from classic ASP.
* Translations: translations come from the database and are cached using Redis for speed.
* Docker: a dockerfile and a dockerignore file are part of the project, making it possible to run this classic ASP application in a docker container. Together with the custom session backend and logging through syslog, it shows how to enable a horizontally scalable classic ASP application in docker.
* Secure cookies and HTTP only cookies in classic ASP. The custom session backend uses secure, httponly cookies in ASP.

#### Prerequisites

This code will not run out-of-the-box. The book will explain chapter-by-chapter how to set things up. Without the book it's harder to get the project up and running, but in short, the following dependencies have to be met:

* You'll of course need to create a classic ASP site for this code in IIS.

* For logging to a file, you need to create a 'log' subfolder, with write-rights for the IUSR that belongs to the application pool for your site. By default the project will log using syslog, but an example of logging to a file is included in the "integration_examples" sub folder.

* You need to install and register the *Redis COM client*, instructions on how to do that can be found here: [https://gitlab.com/erik4/redis-com-client](https://gitlab.com/erik4/redis-com-client)
  
  A precompiled dll to register can be found in the /lib/COM/Redis folder

* You need to install and register the *Syslog COM client*, instructions on how to do that can be found here: [https://gitlab.com/erik4/syslog-com-client](https://gitlab.com/erik4/syslog-com-client)
  
  A precompiled dll to register can be found in the /lib/COM/Syslog folder

* It needs a Redis instance, by default running on the localhost. The project uses Redis for session management and caching. You can run Redis on Windows 10 using the Windows Subsystem for Linux (WSL) using Ubuntu. Detailed instructions are in the book.

* It needs SQL server (Express) running (also by default on the localhost) with a database called "asptest". There are some tables required, for which the `create` scripts are below:

```sql
USE [asptest]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[customer](
    [id] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
    [name] [nvarchar](150) NULL,
    [email] [nvarchar](150) NULL,
    [passwordhash] [char](128) NULL,
    [salt] [char](8) NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF_id]  DEFAULT (newid()) FOR [id]
GO

CREATE TABLE [dbo].[translation](

    [id] [int] IDENTITY(1,1) NOT NULL,
    [label] [nvarchar](max) NOT NULL,
    [language] [varchar](8) NOT NULL,
    [translation] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_translation] PRIMARY KEY CLUSTERED 
(
    [id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

INSERT INTO translation(label, language, translation)
 VALUES ('login','en','Log in')
INSERT INTO translation(label, language, translation)
 VALUES ('login','nl','Inloggen')
INSERT INTO translation(label, language, translation)
 VALUES ('new user','en','New user')
INSERT INTO translation(label, language, translation)
 VALUES ('new user','nl','Nieuwe gebruiker')
INSERT INTO translation(label, language, translation)
 VALUES ('e-mail address','en','E-mail address')
INSERT INTO translation(label, language, translation)
 VALUES ('e-mail address','nl','E-mail adres')
INSERT INTO translation(label, language, translation)
VALUES ('password','en','Password')
INSERT INTO translation(label, language, translation)
 VALUES ('password','nl','Wachtwoord')
INSERT INTO translation(label, language, translation)
 VALUES ('submit','en','Submit')
INSERT INTO translation(label, language, translation)
 VALUES ('submit','nl','Versturen')
```

#### Configuration

The project uses its own method of configuration. If you check the `global.asa` file, you'll see that there are three .config files being loaded. These files contain the configuration variables. They overrule eachother, depending on the order they are loaded.

You will need to change `application.config` to configure the application for your specific situation.

`machine.config` can be used to configure variables specifically for a machine. It assumes that machine.config is not in your version control repository, and that it's unique for every machine the application runs on. `machine.config` can add to or overrule any config files that are loaded before.

`docker.config` is specifically for docker. The `.dockerignore` file will ignore `application.config`, which assures that the settings in `docker.config` are used in a docker environment instead of the settings in `application.config`. For details, please consult the book.

#### License

Copyright (c) 2019 Erik Oosterwaal

The example software is licensed under the MIT License (MIT). See licence.md for details.


