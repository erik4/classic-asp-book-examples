<%
Response.CodePage = 65001 ' 65001 is the codepage for all UTF-8 characters
Response.CharSet = "utf-8"

dim logger
set logger = GetObject("script:" & Server.Mappath("/lib/syslog.wsc"))
    logger.open(Application.Contents("syslog_server"))
    logger.syslogversion = "3164"

dim rSession, connectionerror
set rSession = GetObject("script:" & Server.Mappath("/lib/redissession.wsc"))
On Error Resume Next
    rSession.open()
    connectionerror = Err.Number
On Error goto 0
if connectionerror <> 0 then
    call Err.Raise(462, "redissession.wsc", "Unable to connect to a Redis server for storing session data. This application needs a running Redis instance configured in the redis_session_store application variable.")
end if

dim T
set T = GetObject("script:" & Server.Mappath("/lib/translation.wsc"))
    T.open()

%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
