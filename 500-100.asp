<%
Dim ASPError, logger
Set ASPError = Server.GetLastError
    set logger = GetObject("script:"&Server.MapPath("/lib/syslog.wsc"))
        logger.open(Application.Contents("syslog_server"))
        logger.syslogversion = "3164"
        logger.Critical(ASPError.ASPCode &_
                        ";" & ASPError.ASPDescription &_
                        ";" & ASPError.Category &_
                        ";" & ASPError.Column &_
                        ";" & ASPError.Description &_
                        ";" & ASPError.File &_
                        ";" & ASPError.Line &_
                        ";" & ASPError.Number &_
                        ";" & ASPError.Source)
    set logger = nothing
Set ASPError = Nothing
%>
<%= now() %> - An error has occurred on the server. Administrators have been notified.<br />

