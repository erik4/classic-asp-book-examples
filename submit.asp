<!--#include virtual="inc/header.inc"-->
<%
' Do this only when a from was POSTed
If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
    
    dim Customer, login_worked
    set Customer = GetObject("script:" & Server.Mappath("/lib/datamodels/customer.wsc"))
        Customer.open()
        login_worked = Customer.login(Request.Form("email"), Request.Form("password"))
        if login_worked then
            Response.Write("Welcome " & Customer.Name & "!<br/>")
            rSession.Store "username", Customer.Name
        else
            Response.Status = "401 Access denied"
            Response.Write("Invalid username or password.")
        end if
        Customer.close()
    set Customer = Nothing

End if
%>
<!--#include virtual="inc/footer.inc"-->