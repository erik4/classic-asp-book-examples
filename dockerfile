# The base image is Microsoft's IIS server
FROM microsoft/iis
SHELL ["powershell", "-command"]

# install all of the IIS features we need
RUN Install-WindowsFeature Web-ASP; Install-WindowsFeature Web-ISAPI-Ext; Install-WindowsFeature Web-Mgmt-Service; Install-WindowsFeature Web-Includes; Install-WindowsFeature Web-HTTP-Errors; Install-WindowsFeature Web-Common-HTTP; Install-WindowsFeature Web-Performance; Install-WindowsFeature Web-Basic-Auth; Import-module IISAdministration;
RUN md c:/msi;

#download and install URL rewrite (optional, the sample application doesn't use it) and the ODBC drivers
RUN Invoke-WebRequest 'http://download.microsoft.com/download/C/9/E/C9E8180D-4E51-40A6-A9BF-776990D8BCA9/rewrite_amd64.msi' -OutFile c:/msi/urlrewrite2.msi; Start-Process 'c:/msi/urlrewrite2.msi' '/qn' -PassThru | Wait-Process;
RUN Invoke-WebRequest 'https://download.microsoft.com/download/1/E/7/1E7B1181-3974-4B29-9A47-CC857B271AA2/English/X64/msodbcsql.msi' -OutFile c:/msi/msodbcsql.msi; 
RUN ["cmd", "/S", "/C", "c:\\windows\\syswow64\\msiexec", "/i", "c:\\msi\\msodbcsql.msi", "IACCEPTMSODBCSQLLICENSETERMS=YES", "ADDLOCAL=ALL", "/qn"];

# optional: install and configure remote management of IIS
RUN New-ItemProperty -Path HKLM:\software\microsoft\WebManagement\Server -Name EnableRemoteManagement -Value 1 -Force; Set-Service -Name wmsvc -StartupType automatic;
RUN net user iisadmin Password~1234 /ADD; net localgroup administrators iisadmin /add;

# remove the default website, relay to port 8000 and create a new website bound to port 8000
EXPOSE 8000
RUN Remove-Website -Name 'Default Web Site'; md C:\inetpub\wwwroot\asptest; New-IISSite -Name "asptest" -PhysicalPath 'C:\inetpub\wwwroot\asptest' -BindingInformation "*:8000:";

RUN & c:\windows\system32\inetsrv\appcmd.exe unlock config /section:system.webServer/asp
RUN & c:\windows\system32\inetsrv\appcmd.exe unlock config /section:system.webServer/handlers
RUN & c:\windows\system32\inetsrv\appcmd.exe unlock config /section:system.webServer/modules

ADD . C:\\inetpub\\wwwroot\\asptest

# register the REDIS COM dll
RUN cd C:\inetpub\wwwroot\asptest\lib\COM\Redis\; C:\Windows\Microsoft.NET\Framework64\v4.0.30319\regasm.exe redis-com-client.dll /tlb:redis-com-client.tlb /codebase

# register the SYSLOG COM dll
RUN cd C:\inetpub\wwwroot\asptest\lib\COM\Syslog\; C:\Windows\Microsoft.NET\Framework64\v4.0.30319\regasm.exe SyslogNet.Client.dll /tlb:SyslogNet.Client.tlb /codebase

